# GITLABOPS


## keys and certs
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout gitlab.lightphos.key -out gitlab.lightphos.crt -subj "\/CN=gitlab.lightphos.com" -days 3600

## start up gitlab and reg
mkdir gitlab
cp gitlab*.com.c* gitlab/ssl/

docker volume create gitlab-logs
docker volume create gitlab-data
docker-compose -f docker-compose-gitlab.yml up -d

## runner
docker run -d --name gitlab-runner --restart always \
-v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
-v /Users/Shared/gitlab-runner/certs:/etc/gitlab-runner/certs \
-v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

## register a runner

docker run --rm -t -i -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --url https://gitlab.lightphos.com:30080/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "Runner" \
  --docker-image "docker:19.03.1" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
